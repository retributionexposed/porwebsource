##Note:

When you push code to this repo, the code is automatically uploaded to the site: http://63.142.255.4/
If the code on the site does not update, give it a few seconds.


##Instructions

To create a new page, follow the following steps.

1 make a copy of template.php

2  name the copy whatever you want your new page to be called, make sure it ends with ".php". For example, new_page.php, downloads.php, servers.php, news.php, etc.

3 edit the content in the new file, and replace the place holders with the content you wish to add.

4 Once you have added the content, save it and upload it.

5 You can access the new page as  http://piratesonline.us/downloads  or  http://piratesonline.us/new_page


##API

To use the login api, follow the following steps.

LAUNCHER LOGIN

1 send post request to 63.142.255.4/_t.php with fields user & pass (plaintext)

2 upon error json object with properties {status, code, message}

3 status is either error or success, make a check case for this. A success object has different properties

4 upon success json object with properties {status, code, message, token, ip}

5 status will be success, message user found, token is unique token to be sent to UD, and ip is server ip

UD SERVER CONNECTION

1 send post request to 63.142.255.4/_t.php with the field udtoken (plaintext)

2 udtoken's value should be pulled from path variable set be launcher

3 if error, same type of error json object as launcher

4 upon success, success json object returned with properties {status, code, message, user}

5 first 3 same as before, last property is username for associated token

6 remember to check the value of the status property upon object return as success and error objects have different fields