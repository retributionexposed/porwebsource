<?php include 'db.php';

require_once "recaptchalib.php";

$comment = $conn->real_escape_string($_REQUEST['comment']);
$user = $conn->real_escape_string($_REQUEST['user']);
$id = $conn->real_escape_string($_REQUEST['id']);

$secret = "6Ld86ikTAAAAAPWRtC5ZyexVssd2ola4rNIn4rLn";
$response = null;
$reCaptcha = new ReCaptcha($secret);

if($_POST["g-recaptcha-response"]){
	$response = $reCaptcha->verifyResponse(
		$_SERVER["REMOTE_ADDR"],
		$_POST["g-recaptcha-response"]
		);
}

if($response->success == "1"){
	$sql = "INSERT INTO Tab_NewsComments (UserName, Comment, PostId, Status) VALUES ('".$user."', '" . $comment . "', '".$id."', 0)";
	$result = $conn->query($sql) or die ('Something has gone wrong, try again later');

	header("location: comments.php?m=1&id=".$id);
	exit();
}else{
	header("location: comments.php?e=3&id=".$id);
	exit();
}
?>