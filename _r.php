<?php include 'db.php';

require_once "recaptchalib.php";

function getUserIP() {
	$client  = $_SERVER['HTTP_CLIENT_IP'];
	$forward = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$remote  = $_SERVER['REMOTE_ADDR'];
	if(filter_var($client, FILTER_VALIDATE_IP)) {
		$ip = $client;
	} else if(filter_var($forward, FILTER_VALIDATE_IP))  {
		$ip = $forward;
	} else {
		$ip = $remote;
	}
	
	echo (filter_var($client, FILTER_VALIDATE_IP));
	return $ip;
}

$user = $conn->real_escape_string($_REQUEST['user']);
$email = $conn->real_escape_string(strtolower($_REQUEST['email']));
$pass = $conn->real_escape_string($_REQUEST['pass']);

$pass = password_hash($pass, PASSWORD_DEFAULT);
$address = getUserIP();

$secret = "6Ld86ikTAAAAAPWRtC5ZyexVssd2ola4rNIn4rLn";
$response = null;
$reCaptcha = new ReCaptcha($secret);
if($_POST["g-recaptcha-response"]){
	$response = $reCaptcha->verifyResponse(
		$_SERVER["REMOTE_ADDR"],
		$_POST["g-recaptcha-response"]
	);
}
if($response->success == "1") {
	$sql = "SELECT Address FROM Tab_BannedIP WHERE Address='" . $address . "'";
	$result = $conn->query($sql) or die ('Something has gone wrong, try again later4');

	if($result->num_rows > 0){
		header("location:register.php?e=4");
		exit();
	}
	
	$sql = "SELECT UserName FROM Tab_Reg WHERE UserName='" . $user . "'";
	$result = $conn->query($sql) or die ('Something has gone wrong, try again later3');

	if($result->num_rows > 0){
		header("location:register.php?e=1");
		exit();
	}

	$sql = "SELECT Email FROM Tab_Reg WHERE Email='" . $email . "'";
	$result = $conn->query($sql) or die ('Something has gone wrong, try again later2');

	if($result->num_rows > 0){
		header("location:register.php?e=2");
		exit();
	}

	$verified = bin2hex(random_bytes(64));

	$sql = "INSERT INTO Tab_Reg (id, Email, UserName, T, Verified, Address, ForcePass)
			VALUES (NULL, '".$email."', '".$user."','".$pass."','".$verified."', '".$address."', 1)";

	$conn->query($sql);

	require 'lib/PHPMailer/PHPMailerAutoload.php';

	$host = $_SERVER['HTTP_HOST'];

	$template = file_get_contents(dirname(__FILE__) . '/email/confirm_email.html');
	$template = str_replace('{$host}', $host, $template);
	$template = str_replace('{$token}', $verified, $template);


	//Create a new PHPMailer instance
	$mail = new PHPMailer;

    //Tell PHPMailer to use SMTP
	$mail->isSMTP();

    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
	$mail->SMTPDebug = 0;

    //Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';

    //Set the hostname of the mail server
	$mail->Host = 'smtp.office365.com';
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6

    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
	$mail->Port = 25;

    //Set the encryption system to use - ssl (deprecated) or tls
	//$mail->SMTPSecure = 'tls';

    //Whether to use SMTP authentication
	$mail->SMTPAuth = true;

    //Username to use for SMTP authentication - use full email address for gmail
	$mail->Username = $emailUser;

    //Password to use for SMTP authentication
	$mail->Password = $emailPass;

    //Set who the message is to be sent from
	$mail->setFrom($emailUser, 'Pirates Online Retribution');

    //Set an alternative reply-to address
	//$mail->addReplyTo('support@piratesforums.us', 'Pirates Forums');
    //Set who the message is to be sent to
	$mail->addAddress($email, $fn . ' ' . $ln);

    //Set the subject line
	$mail->Subject = 'Welcome! Please confirm your email at Pirates Online Retribution';

    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
	$mail->msgHTML($template, dirname(__FILE__));

    //Replace the plain text body with one created manually
	$mail->AltBody = 'Welcome!, Please confirm your email.';

	//send the message, check for errors
	if (!$mail->send()) {
		echo "Error: " . $mail->ErrorInfo;
	} else {
		header("location: confirm.php?email=".$email);
		exit();
	}
}else{
	header("location: register.php?e=3");
	exit();
}
?>
