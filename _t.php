<?php
include 'db.php';
include 'timeUtils.php';

header('Content-Type: application/json');

function getUserIP()
{
    $client  = $_SERVER['HTTP_CLIENT_IP'];
    $forward = $_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];
    if (filter_var($client, FILTER_VALIDATE_IP)) {
        $ip = $client;
    } else if (filter_var($forward, FILTER_VALIDATE_IP)) {
        $ip = $forward;
    } else {
        $ip = $remote;
    }
    
    return $ip;
    
}

$user    = $conn->real_escape_string($_POST['user']);
$pass    = $conn->real_escape_string($_POST['pass']);
$address = getUserIP();
$udtoken = $conn->real_escape_string($_POST['udtoken']);

$sql    = "SELECT Address FROM Tab_BannedIP WHERE Address='" . $address . "'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo json_encode(array(
        'status' => 'error',
        'code' => '2',
        'message' => 'The IP: ' . $address . ' is banned!'
    ));
    exit(0);
}

if ($udtoken != "") {
    $sql    = "SELECT UserName, Access FROM Tab_Reg WHERE Temp='" . $udtoken . "' AND TempExpire > " . time() . " AND Verified='1'";
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
        $row    = $result->fetch_assoc();
        $user   = $row['UserName'];
        $access = $row['Access'];
        
        $conn->query("UPDATE Tab_Reg SET Temp=NULL, TempExpire=NULL WHERE UserName='" . $user . "' AND Verified='1'");
        
        echo json_encode(array(
            'status' => 'success',
            'code' => '0',
            'message' => 'User found',
            'user' => $user,
            'access' => $access
        ));
    } else {
        echo json_encode(array(
            'status' => 'error',
            'code' => '1',
            'message' => 'No matching user.'
        ));
    }
    
    exit(0);
}

$sql    = "SELECT BannedUntil, T, ForcePass, Email, Verified, RankExpire FROM Tab_Reg WHERE UserName='" . $user . "'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    $row         = $result->fetch_assoc();
    $bannedUntil = (int) $row['BannedUntil'];
    $verified    = (int) $row['Verified'];
    
    if ($verified !== 1) {
        echo json_encode(array(
            'status' => 'error',
            'code' => '4',
            'message' => 'Your account has not been verified yet! Please check your email for instructions.'
        ));
        exit(0);
    }
    
    if (is_numeric($bannedUntil)) {
        $bannedUntil = (int) $bannedUntil;
        $banned      = false;
        
        if ($bannedUntil === 1) {
            $banDuration = 'forever';
            $banned      = true;
        } else if ($bannedUntil >= time()) {
            $banDuration = getHumanTime($bannedUntil - time());
            $banned      = true;
        }
        
        if ($banned) {
            echo json_encode(array(
                'status' => 'error',
                'code' => '2',
                'message' => 'You are currently banned for ' . $banDuration . '!'
            ));
            exit(0);
        }
    }
    
    if (password_verify($pass, $row['T'])) {
        $forcePass = (int) $row["ForcePass"];
        if ($forcePass === 1) {
            $rankExpire = (int) $row["RankExpire"];

            if ($rankExpire > 0 && $rankExpire <= time()) {
                $sql = "UPDATE Tab_Reg SET RankExpire=NULL, Access=100 WHERE UserName='" . $user . "'";
                $conn->query($sql);
            }

            $token      = bin2hex(random_bytes(64));
            $timeExpire = time() + (15 * 60);
            $sql        = "UPDATE Tab_Reg SET Temp='" . $token . "', Address='" . $address . "', TempExpire=" . $timeExpire . " WHERE UserName='" . $user . "' AND Verified='1'";
            $result     = $conn->query($sql);
            $ip         = '149.56.99.235';
            echo json_encode(array(
                'status' => 'success',
                'code' => '0',
                'message' => 'User found',
                'token' => $token,
                'ip' => $ip
            ));
        } else {
            $resetToken  = bin2hex(random_bytes(64));
            $resetExpire = PHP_INT_MAX;
            
            $sql = "UPDATE Tab_Reg SET ResetToken='" . $resetToken . "', ResetExpire=" . $resetExpire . " WHERE UserName='" . $user . "'";
            $conn->query($sql);
            
            require 'lib/PHPMailer/PHPMailerAutoload.php';
            
            $host     = $_SERVER['HTTP_HOST'];
            $email    = $row['Email'];
            $template = file_get_contents(dirname(__FILE__) . '/email/force_reset_email.html');
            $template = str_replace('{$host}', $host, $template);
            $template = str_replace('{$token}', $resetToken, $template);
            
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SMTPDebug   = 0;
            $mail->Debugoutput = 'html';
            $mail->Host        = 'smtp.office365.com';
            $mail->Port        = 25;
            $mail->SMTPAuth    = true;
            $mail->Username    = $emailUser;
            $mail->Password    = $emailPass;
            $mail->setFrom($emailUser, 'Pirates Online Retribution');
            $mail->addAddress($email, $fn . ' ' . $ln);
            $mail->Subject = 'Mandatory Password Reset for Pirates Online Retribution';
            $mail->msgHTML($template, dirname(__FILE__));
            $mail->AltBody = 'Please reset your password!';
            $mail->send();
            echo json_encode(array(
                'status' => 'error',
                'code' => '3',
                'message' => 'Avast! For security purposes, please be sure to update your password prior to logging in again. An email has just been sent to your email address with instructions on how to do so. Once your password has been updated, you should be able to get right back into the game and start plundering again. We apologize for any inconvenience this may have caused, and hope to see you back on the High Seas very soon!'
            ));
        }
    } else {
        echo json_encode(array(
            'status' => 'error',
            'code' => '1',
            'message' => 'Incorrect username or password.'
        ));
    }
} else {
    echo json_encode(array(
        'status' => 'error',
        'code' => '1',
        'message' => 'Incorrect username or password.'
    ));
}
?>

