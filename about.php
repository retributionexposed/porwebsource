<?php $options['no_content_page'] = true; ?><?php include 'sections/header.php'; ?>

            <style type="text/css">

                .container {
                width: 750px;
                }
                .container, .container-fluid {
                margin-right: auto;
                margin-left: auto;
                padding-left: 15px;
                padding-right: 15px;
                }

                .col-sm-3, .container, .container-fluid :after, :before {
                box-sizing: border-box;
                }

                .row:after, .row:before {
                content: " ";
                display: table;
                }

                .row {
                margin-left: -15px;
                margin-right: -15px;
                }


                .col-sm-3 {
                position: relative;
                min-height: 1px;
                padding-left: 15px;
                padding-right: 15px;
                 float: left;
                width: 33%;
                }

                .staff-profile {
                text-align: center;
                margin-top: 79px;Y
                position: relative;
                margin-bottom: 40px;
                }

                .staff-profile h3 {
                font-size: 20px;
                margin: 15px 0 7.5px;
                }

                .staff-profile h3 small {
                display: block;
                font-size: 16px;
                color: #920606;
                }


                .staff-profile img {
                margin-top: -67px;
                border-radius: 50%;
                position: relative;
                z-index: 10;
                width: 128px;
                height: 128px;
                }


                .page-content {
                 min-height: 100%;
                margin-bottom: -110px;
                overflow: hidden;
                }
                .content_page {
                position: relative;
                background-repeat: repeat-y;
                width: 800px;
                line-height: 30px;
                object-fit: fill;
                margin: 0 auto 0 auto;
                z-index: 0;
                margin-top: -100px;
                padding: 100px 10px 0 50px;
                text-align: left;
                }
            </style>
            <title></title>
        </head>
        <body>
            <br>
            <center>
                <h1 class="silver_bar_left" style="width: 400px; font-size: 33px;">
                PIRATEOGRAPHY</h1>
            </center>
            <center>
                <h2>How it all started...</h2>
            </center>
            <center>
                <div class="box350" style="width: 725px;">
                Pirates Online Retribution officially entered its Open Alpha stage of development in September, 2016. Since then we've made countless content updates and improvements
to our swashbuckling MMO, including looting, guilds, caves, improved enemy AI, and much more! We offer a lag-free, 100% free to play, family friendly gaming environment 
with frequent content updates, and a very hands-on and attentive staff. We put our community first, and greatly value our player's input and feedback, because we
recognize and understand that they are what ultimately helped to make Pirates Online the legendary game that it is today!
                </div>
            </center><br>
            <br>
            <br>
            <br>
            <center>
                <h1 class="silver_bar_left" style=
                "width: 400px; font-size: 33px; text-align: center;">The Crew</h1>
            </center>
            <div class="container">
                <div class="row" data-fitcache-eheight="334">
                    <div class="col-sm-3">
                        <div class="staff-profile" data-department="" data-fit-for=
                        ".staff-profile in .row" style="height: 349px;">
                            <img src="images/pearson1.jpg">
                            <h3>Pearson W <small>Founder and Owner</small></h3>
                            <p></p>
                            <center style=
                            "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                            The guild guru of Pirates Online, Pearson founded Pirates
                            Online Retribution in June of 2016 with Noah Rochefort and
                            a small group of talented and dedicated developers. He’s
                            unshakable in his mission to see Pirates Online fully
                            restored to the former POTCO community in its full and
                            public form! In his free time, you’ll find him studying,
                            overseeing communities, and innovating upon old ideas.
                                <p></p>
                            </center>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="staff-profile" data-department="" data-fit-for=
                        ".staff-profile in .row">
                            <a id="Test" name="Test"></a> <img alt="Testt" src=
                            "images/noah.jpg">
                            <h3>Noah R <small>Co. Owner</small></h3>
                            <p></p>
                            <center style=
                            "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                            The master of PvP on Pirates Online, Noah, also known
                            simply as “Hippie” was an iconic PvPer and Youtuber
                            throughout much of POTCO’s existence. Today, he hosts
                            tournaments and other events
                            on Pirates Online Retribution, and creates informative
                            and engaging videos on the POR Youtube Channel.
                                <p></p>
                            </center>
                            <div class="staff-social"></div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="staff-profile" data-department="" data-fit-for=
                        ".staff-profile in .row" style="height: 349px;">
                            <a id="Matt" name="Matt"></a> <img alt="Matt" src=
                            "images/sips.png">
                            <h3>Kyle S <small>Project Overseer</small></h3>
                            <p></p>
                            <center style=
                            "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                            One of the oldest members of the POR Staff team; Kyle, also known as "Dr. Sips" is responsible for ensuring that all aspects of the project are being managed
                                        appropriately, and professionally. You can often find him lurking on the Pirates Forums, or the POR Discord.
                                <p></p>
                            </center>
                        </div>
                    </div><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="row" data-fitcache-eheight="334">
                        <div class="col-sm-3">
                            <div class="staff-profile" data-department="" data-fit-for=
                            ".staff-profile in .row">
                                <a id="Test" name="Test"></a> <img alt="Testt" src=
                                "images/hallagon.jpg">
                                <h3>Hallagon <small>Lead Game Developer</small></h3>
                                <p></p>
                                <center style=
                                "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                                The real brains behind the project, Hallogan joined
                                Pirates Online Retribution in August of 2016 after
                                being recruited by Pearson Wright. Since then, he’s
                                worked tirelessly to make POR the great project that it
                                is today. In his free time, you’ll find him fixing
                                bugs, implementing new features on POR, playing games,
                                and studying.
                                    <p></p>
                                </center>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="staff-profile" data-department="" data-fit-for=
                            ".staff-profile in .row">
                                <a id="Test" name="Test"></a> <img alt="Testt" src=
                                "images/beard.jpg">
                                <h3>Christian H <small>Chief of Staff</small></h3>
                                <p></p>
                                <center style=
                                "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                                Christian, known more commonly as "Beardynator" is is responsible for coordinating with all levels of POR's Staff,
                                as well as handling recruitment, promotions, demotions, and assisting
                                the project manager in ensuring that every member of the POR Staff is
                                adequately fulfilling his or her obligations as a member of the crew.
                                    <p></p>
                                </center>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="staff-profile" data-department="" data-fit-for=
                            ".staff-profile in .row">
                                <a id="Test" name="Test"></a> <img alt="Testt" src=
                                "https://piratesonline.us/images/skye.png">
                                <h3>Zachary B<small>Lead Web Developer</small></h3>
                                <p></p>
                                <center style=
                                "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                                Another senior member of the POR Staff; Zachary leads all web development, database development, and API development related tasks here at POR.
                                He also manages other operations such as database and account security. In his free time, you'll find him studying, and
                                creating/managing databases for other tech companies.
                                    <p></p>
                                </center>
                            </div>
                        </div>

                       <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div class="col-sm-3">
                                <div class="staff-profile" data-department="" data-fit-for=
                                ".staff-profile in .row">
                                    <a id="Test" name="Test"></a> <img alt="Testt" src=
                                    "images/fade.jpg">
                                    <h3>Kellen S<small>Lead Global Moderator</small></h3>
                                    <p></p>
                                    <center style=
                                    "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                                    Kellen, known more commonly as "Fade" is the chief moderator of POR's forums, gameservers, and discord, and a very loyal
                                                and long-serving member of the POR community. He is responsible for overseeing and coordinating with all of the subordinate
                                                moderators here at POR. In his free time, you'll find him conversing in the Discord, and hosting GM events in-game.
                                        <p></p>
                                    </center>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="staff-profile" data-department="" data-fit-for=
                            ".staff-profile in .row">
                                <a id="Test" name="Test"></a> <img alt="Testt" src=
                                "images/glare.png">
                                <h3>Glare Masters<small>Web Developer</small></h3>
                                <p></p>
                                <center style=
                                "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                                Glare is an extremely hard-working, and dedicated staff member of POR who helps develop and
                                manage the POR Website, POR Forums, Pirates MC Website, and handles other various small tasks
                                for the project such as the POR Newsletter, game and web server firewalls, and Discord bots.
                                    <p></p>
                                </center>
                            </div>
                        </div>
                        <div class="col-sm-3">
                        <div class="staff-profile" data-department="" data-fit-for=
                        ".staff-profile in .row">
                            <a id="Test" name="Test"></a> <img alt="Testt" src=
                            "images/gundi.jpg">
                            <h3>Jeffrey T<small>Media Team Manager</small></h3>
                            <p></p>
                            <center style=
                            "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                            Jeffrey, also known as "YoGundi" is an extremely charismatic, and creative member of the POR Staff team. He is responsible for overseeing
                            all of POR's content on Youtube, and also helps manage both the POR Youtube channel and his own personal channel; "YoGundi". In his free
                            time, you'll find him brainstorming new ideas for POR's livestreams and cinematics, and playing paintball.
                                <p></p>
                            </center>
                        </div>
                    </div><br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="col-sm-3">
                        <div class="staff-profile" data-department="" data-fit-for=
                        ".staff-profile in .row">
                            <a id="Test" name="Test"></a> <img alt="Testt" src=
                            "images/danny.jpg">
                            <h3>Daniel A <small>Lead Game Designer</small></h3>
                            <p></p>
                            <center style=
                            "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                            Daniel Alves is in charge of all of the 3D modeling projects here
                            at POR, ranging from custom items in-game, to cinematic videos showcasing new content. With his help, POR has become
                                        more than just an emulator of Pirates Online; and has instead emerged in a way as a game of its own.
                                <p></p>
                            </center>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="staff-profile" data-department="" data-fit-for=
                        ".staff-profile in .row">
                            <a id="Test" name="Test"></a> <img alt="Testt" src=
                            "images/natsu.jpg">
                            <h3>Brannon M<small>Lead Forums Moderator</small></h3>
                            <p></p>
                            <center style=
                            "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                            Brannon, also known as "Natsu" is a very well-liked, and active member of the POR community who frequently hosts GM events and
                                        does collaboration videos with other POR Youtubers such as Aaron Crayon and YoGundi. You can often find him moderating
                                        the POR Forums and/or Discord, or hanging out on the docks of Tortuga, Abassa.
                                <p></p>
                            </center>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="staff-profile" data-department="" data-fit-for=
                        ".staff-profile in .row">
                            <a id="Test" name="Test"></a> <img alt="Testt" src=
                            "images/commander.jpg">
                            <h3>Jacob W.<small>Lead Discord Moderator</small></h3>
                            <p></p>
                            <center style=
                            "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                            Jacob, also known as "Commander", or "Commander Jake", like Natsu is also a very active member of the POR community who frequently helps host
                                        GM events in-game and works tirelessly to ensure that the rules are adequately enforced on the game, forums, and discord. In his free time,
                                        you may find him occasionally handing out items in-game, or hanging out in voice chat on Discord.
                                <p></p>
                            </center>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                       <div class="col-sm-3">
                        <div class="staff-profile" data-department="" data-fit-for=
                        ".staff-profile in .row">
                            <a id="Test" name="Test"></a> <img alt="Testt" src=
                            "images/fafa,jpg.jpg">
                            <h3>Noah C <small>Lead Support Representative</small></h3>
                            <p></p>
                            <center style=
                            "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                            Noah, also known as "TheOriginalFaFa" is the lead community support representative here at POR, and handles a variety of tasks
                                        ranging from responding to customer support emails and inquiries, to answering questions on the forums, and discord. If
                                        you have any bugs to report, or comments or concerns regarding POR, don't hesitate to shoot him a message!
                                <p></p>
                            </center>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="staff-profile" data-department="" data-fit-for=
                        ".staff-profile in .row">
                            <a id="Test" name="Test"></a> <img alt="Testt" src=
                            "images/rachel.jpg">
                            <h3>Rachel P <small>Lead Greeter</small></h3>
                            <p></p>
                            <center style=
                            "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                            Rachel, also known by most simply as "Ducky" is the head of the Greeter department here at POR, and is responsible for
                                        ensuring that everyone who joins our community is welcomed warm-heartedly and with open arms, and that they
                                        are able to fully enjoy and experience all that POR has to offer. In her free time, you may find here hanging
                                        out with other members of the POR community in the POR Discord.
                                <p></p>
                            </center>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="staff-profile" data-department="" data-fit-for=
                        ".staff-profile in .row">
                            <a id="Test" name="Test"></a> <img alt="Testt" src=
                            "images/adrianna.jpg">
                            <h3>Adrianna E<small>Global Moderator</small></h3>
                            <p></p>
                            <center style=
                            "font-size: 16px; margin: 0; padding: 0; line-height: 22px;">
                                Adrianna is a very long-serving and committed member of the POR staff team. She helps moderate the game, discord, and POR forums,
                                        and leads one of POR's largest and most active guilds; "Adrianna's Angels". In her free time, you may find her hangdueling out
                                        on the Forums or dueling enemies in-game.
                                <p></p>
                            </center>
                        </div>
                    </div>
                </div>
            </div><br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
                    <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                                        <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <?php include 'sections/footer.php'; ?>
