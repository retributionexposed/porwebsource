<?php include 'sections/header.php';

$email = $conn->real_escape_string($_REQUEST['email']);
?>
<div class="page_center">
    <h1>Welcome to the High Seas!</h1>

    <h2>Just one more thing</h2>

    <p>
    To finalize registration, please see the confirmation email sent to the address entered previously. If you did not get an email, please check your spam folder or try registering again. Thank you!
    </p>
    <br>
    Please Note: Emails can take several minutes to send. Please be patient!<br><br>
    <form id="Form1" action="_rs.php" method="post">
    <input type="hidden" id="email" name="email" value="<?php echo $email;?>">
    Please be patient, but if your email doesn't arrive, please <a href="#" onClick="Resend();return false;">click here</a> to resend<br><Br>
    <p>
    Once you have confirmed your email address you can use your username and password to sign into the game client, and sign in above to change any account information.
    </p>
</div>
<script>
function Resend(){
    $('#Form1').submit();
}
</script>
<?php include 'sections/footer.php';?>