<?php include 'sections/header.php'; ?>

<div class="page_center">

    <h1>Contact US!</h1>

    <img src="images/table.png" />

    <p>
        Feel free to submit a ticket to us at any time by filling out your credentials below, and submitting a message to our customer support staff. Whether you're reporting a bug, applying for a staff position, making a suggestion, or asking an overall question about Pirates Online Retribution, this is the fastest and easiest way to let us know what's on your mind!

    </p><br><br>
    <form action="https://formspree.io/support@piratesonline.us" method="POST">
      <div style="width:535px;" align="right">
  						<label>UserName</label>
  						<input type="text" name="name"><br><br>
  						<label>Email</label>
  						<input type="email" name="email"><br><br>
  						<label>Subject</label>
  						<input type="text" name="subject"><br><br>
  						<label style="position: relative; top: -24px;">Details</label>
  						<textarea name="message" style="padding-right:70px;"></textarea><br><br>
  						<input type="text" name="_gotcha" style="display:none">
  						<button type="submit" class="button-chk"><span>Submit</span></button></div>
  					</form>
</div><br><br><br><br><br><br>
<?php include 'sections/footer.php'; ?>
