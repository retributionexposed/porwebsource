<?php include '../db.php';
header('Content-Type: application/json');

if (!isset($_POST['name']) || !isset($_POST['secrettoken'])) {
	echo json_encode(array('status' => 'error', 'code' => '0', 'message' => 'What do you mean?'));
	exit(0);
}

if ($secrettoken !== $_POST['secrettoken'] || $_SERVER['HTTP_USER_AGENT'] !== 'POR-ClientServicesManagerUD') {
	echo json_encode(array('status' => 'error', 'code' => '1', 'message' => 'Who are you?'));
	exit(0);
}

$name = $conn->real_escape_string($_POST['name']);

$sql = "SELECT Status FROM Tab_NameWait WHERE Name='" . $name . "'";
$result = $conn->query($sql);

if($result->num_rows > 0){
	$row = $result->fetch_assoc();
	echo json_encode(array('status' => 'success', 'code' => '2', 'nameStatus' => $row["Status"], 'message' => 'Pulled name status.'));
	exit(0);
}

$sql = "INSERT INTO Tab_NameWait (Name, Status) VALUES ('" . $name . "', 0)";
$conn->query($sql);
echo json_encode(array('status' => 'success', 'code' => '2', 'nameStatus' => '0', 'message' => 'Sent name for review.'));
?>
