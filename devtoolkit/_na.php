<?php include '../db.php';
header('Content-Type: application/json');

$sql = "SELECT id, Title, Description, Link, Date FROM Tab_LauncherNews ORDER BY id DESC";
$result = $conn->query($sql) or die("something has gone wrong");

$jsonData = array();
while ($row = $result->fetch_assoc()) {
    $jsonData[] = $row;
}
echo json_encode($jsonData);
?>
