<?php 
include '../db.php'; 

session_start();
$sql = "CALL CheckAccessLevel('" . $conn->real_escape_string($_SESSION["id"]) . "', '900')";
$result = $conn->query($sql);
$conn->next_result();

if($result->num_rows == 0){
    header("location:login.php");
    exit();
}

if(!empty($_POST['id'])) {
    $sql = "CALL GetBannerAlertsByID ('".$conn->real_escape_string($_POST['id'])."')";
	$result = $conn->query($sql);
	$row = $result->fetch_assoc();
    $conn->next_result();
    echo json_encode(array("alert"=>$row['Alert']));
    exit;
}

include '../sections/head.php';
?>
    <a href="main.php">Back</a><br><br><Br>
    <form id="Form1" action="_al.php" method="post">
        <input type="hidden" id="title" name="title" value="" />
    	<select id="selections" name="selections">
		  <option value="new">New Alert</option>
		  <?php
		  	$sql = "CALL GetBannerAlerts";
			$result = $conn->query($sql);
            $conn->next_result();

			if($result->num_rows > 0){
	            while($row = $result->fetch_assoc()) {
	                echo "<option value='".$row["id"]."'>".$row["Alert"]."</option>";
	            }
        	}	
		  ?>
		</select>
		<div style="margin:5px;"></div>
       	<label>Alert: </label><input type="text" id="alert" name="alert" style="width:250px;"><br><br>
      	<input type="button" onClick="Validate(1);" value="Create/Update" />
      	<input type="button" onClick="Validate(2);" value="Delete" />
    </form>
    <?php
        if($_GET["m"] == 1){
    ?>
            <h2><font color="red">Alert Added</font></h2>
    <?php
        }else if($_GET["m"] == 2){
            echo '<h2><font color="red">Alert Deleted</font></h2>';
        }else if($_GET["m"] == 3){
            echo '<h2><font color="red">Alert Updated</font></h2>';
        }
    ?>
<script>
$('#selections').change(function(){ 
    var value = $(this).val();

    if(value == "new"){
    	$("#alert").val("");
    }else{
    	$.ajax({
            url: 'alerts.php',
            type: 'post',
            
            data: { id: value },
            success: function(response) {
                var Vals = JSON.parse(response);
                $("#alert").val(Vals.alert);
            }
        });
    }
});

function Validate(type){
	var flgIsValid = false;
	var e = "";

	if(type == 1){
		if($('#alert').val() == ""){
			e = e + "\nPlease Enter Alert";
		}

		if(e == ""){
			flgIsValid = true;
		}

		if(flgIsValid){
			$('#Form1').submit();
		}else{
			alert("There are the following issues:" + e);
		}
	}else{
		$("#title").val("forigu");
		$('#Form1').submit();
	}
}
</script>