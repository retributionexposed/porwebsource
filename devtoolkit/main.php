<?php
include '../sections/head.php';
include '../db.php';

session_start();
$sql = "CALL CheckAccessLevel('" . $conn->real_escape_string($_SESSION["id"]) . "', '900')";
$result = $conn->query($sql);
$conn->next_result();

if($result->num_rows > 0){
    $row = $result->fetch_assoc();
?>
<link rel="shortcut icon" href="../favicon.ico">
<link rel="stylesheet" type="text/css" href="css/normalize.css" />
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<link rel="stylesheet" type="text/css" href="css/component.css" />
<script src="js/modernizr.custom.25376.js"></script>
<ul>
  <body>
    <div id="perspective" class="perspective effect-rotateleft">
      <div class="container">
        <div class="wrapper"><!-- wrapper needed for scroll -->
          <!-- Top Navigation -->
          <header class="codrops-header">
            <h1>Pirates Online Retribution <span>Relive the Legend</span></h1>
          </header>
          <div class="main clearfix">
            <center>
              <p><button id="showMenu" style="min-height: 100px; width: 50%; padding: 0 2em; position: relative;">Show Menu</button></p>
          </div><!-- /main -->
        </div><!-- wrapper -->
      </div><!-- /container -->
      <nav class="outer-nav right vertical">
        <a href="approval.php" class="icon-lock">Name Approval</a>
        <a href="guildapproval.php" class="icon-lock">Guild Name Approval</a>
        <a href="commentapproval.php" class="icon-lock">Comment Approval</a>
        <a href="news.php" class="icon-news">Website / Launcher News</a>
        <a href="alerts.php" class="icon-news">Website Alerts</a>
        <a href="botmessages.php" class="icon-mail">Discord Bot Messages</a>
        <a href="bannedips.php" class="icon-star">IP Ban Panel</a>
<?php
	if($row['Access'] == 1000){
?>
<a href="databaseconfig.php" class="icon-upload">Database Management</a>
</nav>
</div><!-- /perspective -->
<script src="js/classie.js"></script>
<script src="js/menu.js"></script>
</body>
<?php
	}
	$sql = "CALL GetVerifiedAccountCount";
	$result = $conn->query($sql);
	$amount = $result->fetch_assoc()['RegCount'];
	$conn->next_result();
	echo "<br><br><br> Verified Registrants: " . $amount;
}else{
	header("location:login.php");
	exit();
}
?>
</html>
