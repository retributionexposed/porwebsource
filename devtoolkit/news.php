<?php 
include '../db.php'; 

session_start();
$sql = "CALL CheckAccessLevel('" . $conn->real_escape_string($_SESSION["id"]) . "', '900')";
$result = $conn->query($sql);
$conn->next_result();

if($result->num_rows == 0){
    header("location:login.php");
    exit();
}

if(!empty($_POST['id'])) {
    $sql = "CALL GetNewsByID ('".$conn->real_escape_string($_POST['id'])."')";
	  $result = $conn->query($sql);
	  $row = $result->fetch_assoc();
    $conn->next_result();
    echo json_encode(array("title"=>$row['Title'],"desc"=>$row['Description'],"date"=>$row['Date'],"message"=>$row['Message']));
    exit;
}

include '../sections/head.php';
?>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
    tinymce.init({ selector:'textarea',
                   plugins : 'advlist autolink link image lists charmap print preview',
                   theme: 'modern',
                   width: 600,
                   height: 500,
                   file_browser_callback_types: 'file image media',
                   automatic_uploads: false
   });
   </script>
    <a href="main.php">Back</a><br><br><Br>
    <form id="Form1" action="_n.php" method="post">
    	<select id="selections" name="selections">
		  <option value="new">New Post</option>
		  <?php
		  	$sql = "CALL GetNews";
			$result = $conn->query($sql);
      $conn->next_result();

			if($result->num_rows > 0){
	            while($row = $result->fetch_assoc()) {
	                echo "<option value='".$row["id"]."'>".$row["Title"]."</option>";
	            }
        	}	
		  ?>
		</select>
		<div style="margin:5px;"></div>
       	<label>Title: </label><input type="text" id="title" name="title"><br>
       	<div style="margin:5px;"></div>
    	<label>Description: </label><input type="text" id="desc" name="desc"><br>
    	<div style="margin:5px;"></div>
    	<label>Date: </label><input type="text" id="date" name="date"><br>
        <div style="margin:5px;"></div><br>
        <label>News post: </label><textarea id="newspost" name="newspost"></textarea><br><br>
        -Only use images with a width up to 500px<br>
        -Link from imgur or gyazo in the image upload thingy<br>
        -You may style text however you'd like, but it is suggested<br>
        that text (and images) are center aligned. The site will auto-bold<br>
        all text for stylistic purposes.
      	<br><br>
      	<input type="button" onClick="Validate(1);" value="Create/Update" />
      	<input type="button" onClick="Validate(2);" value="Delete" />
    </form>
    <?php
        if($_GET["m"] == 1){
    ?>
            <h2><font color="red">Post Added</font></h2>
    <?php
        }else if($_GET["m"] == 2){
            echo '<h2><font color="red">Post Deleted</font></h2>';
        }else if($_GET["m"] == 3){
            echo '<h2><font color="red">Post Updated</font></h2>';
        }
    ?>
<script>
$('#selections').change(function(){ 
    var value = $(this).val();

    if(value == "new"){
    	$("#title").val("");
    	$("#desc").val("");
    	$("#date").val("");
      tinymce.get('newspost').setContent("");
    }else{
    	$.ajax({
            url: 'news.php',
            type: 'post',
            
            data: { id: value },
            success: function(response) {
                var Vals = JSON.parse(response);
                
                $("#title").val(Vals.title);
      		    	$("#desc").val(Vals.desc);
      		    	$("#date").val(Vals.date);
                tinymce.get('newspost').setContent(Vals.message);
            }
        });
    }
});

function Validate(type){
	var flgIsValid = false;
	var e = "";

	if(type == 1){
		if($('#title').val() == ""){
			e = e + "\nPlease Enter Title";
		}

		if($('#desc').val() == ""){
			e = e + "\nPlease Enter Description";
		}

		if($('#date').val() == ""){
      e = e + "\nPlease Enter Date";
    }

    if(tinymce.get('newspost').getContent() == ""){
      e = e + "\nPlease Enter Message";
    }

		if(e == ""){
			flgIsValid = true;
		}

		if(flgIsValid){
			$('#Form1').submit();
		}else{
			alert("There are the following issues:" + e);
		}
	}else{
		$("#title").val("forigu");
		$('#Form1').submit();
	}
}
</script>