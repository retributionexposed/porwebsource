<?php $options['no_content_page'] = true; ?>
<?php include 'sections/header.php';
?>

<div class="page_center">
  <div class="video"> <iframe width="560" height="315" src="https://www.youtube.com/embed/SauoS2eUgac?autoplay=1" frameborder="0" allowfullscreen></iframe></div>



    <p class="box400 center">
     Welcome to the <b>Pirates Online Retribution</b> project! We are a group of
        highly dedicated developers, financiers, and designers hellbent
        on resurrecting the now defunct popular MMO, Pirates of the Caribbean Online.
        Together, we are restoring lost hope to a very special community. Retribution
        is at hand!
    </p>

    <br/>
    <div class="box550 center550 p">
        <div class="box230 left">
            <ul class="book">
                <li> Hosted on six <i>Intel Dual Xeon E5-2630v3</i> servers to ensure optimized performance!</li>

                <li> Fully certified game developers, and design artists!</li>

                <li> 100% Free to Play, and always will be!</li>
            </ul>
        </div>
        <div class="box230 right">
            <ul class="book">
                <li>Quality customer support from our admins and moderators!</li>

                <li>Top of the line DDOS and Hack protection firewalls to ensure 100% uptime!</li>

                <li>Frequent content updates, and server maintenance</li>
            </ul>
        </div>
        <div style="clear: both"></div>
    </div>

    <br/>
    <p class="box400 center">
        Battle against the undead legions of Jolly Roger. Create your own crew or guild. Plunder enemy vessels on the high seas, and seek to become the most notorious pirate in the Caribbean all over again! Are ye' finally ready to relive the legend?
    </p>

<!--
    <h1 class="skull_large">The Heart of Padres!</h1>

    <div class="box400 center">
       <div class="news-date">
           Claim yers before
           its too late!
           <br style="margin: 0;padding: 0" />
           October 4th, 2016
       </div>
    </div>

    <br style="margin: 0;padding: 0"/>

    <img src="https://i.gyazo.com/12d295e78fec61d34118c889bb2c2064.jpg" />

    <br style="margin-bottom: 25px;padding: 0" />

    <div class="box500 center500">
        <div style="font-weight: bold; line-height: 23px; font-family: 'sans-serif'">
           Ahoy, mateys!

Recently thar has been trouble brewin' over on the volcanic island of Padres Del Fuego.
Lord Cutler Beckett and those blimey East India Company thugs have taken control of the mines
and are reapin' themselves a mighty fortune from its rich gem deposits buried deep within the
Heart of the island.

Legend has it, a curse was conjured up hundreds of years ago by the natives of the island
to dispel any and all invaders from ever layin' claim to it. Although long thought
to be not but a myth or a talk of the trade, it would seem that thar may be a bit of truth
to the tale after all. The local gypsies of the island have given me permission to share with ye all
a code that'll allow ye to lay claim to one of only a handful of the cursed swords left from
all those years ago. Thar be a catch though. In return, ye be expected to aid the locals in
helping clear out all those damned Navy and EITC invaders!

Ye can redeem the followin' code:

"Heart of Padres" for the <b>Heart of Padres Sword</b>

This be a mighty powerful weapon mates. Only the best of the best can wield her! We think ye be
up to the task.
        </div>
        <br style="margin-bottom: 0px;padding: 0"/>
        <span style="font-family: 'sans-serif'; font-weight: bold; font-size: 15px">Fair Winds</span>
        ​<br style="margin-bottom: 25px;padding: 0" />
        ~<span style="font-family: 'sans-serif'; font-weight: bold; font-size: 15px"> <i>The Crew @ Pirates Online Retribution</i></span>
        <br style="margin-bottom: 8px;padding: 0"/>
        <a href="http:/piratesforums.us" target="_blank">
            <img src="images/test_server_invite.png" />
        </a>
        <div style="position: relative; padding: 15px"></div>
    </div> -->
    <?php
        $sql = "SELECT * FROM Tab_LauncherNews ORDER BY id DESC";
        $result = $conn->query($sql) or die('Something has gone wrong, try again later');

        if($result->num_rows > 0){
            $row = $result->fetch_assoc();
        //  while($row = $result->fetch_assoc()) {
            $sql = "SELECT id FROM Tab_NewsComments WHERE Status=2 AND PostId=".$row['id'];
            $result2 = $conn->query($sql) or die('Something has gone wrong, try again later');
            echo '<h1 class="skull_large" id="'.$row['id'].'" name="'.$row['id'].'">'.$row['Title'].'</h1>';
            echo '<div class="box400 center"><div class="news-date">'.$row['Description'].'<br style="margin: 0;padding: 0" />'.$row['Date'].'<br style="margin: 0;padding: 0" />';
            if($result2->num_rows > 0){
                $row2 = $result2->fetch_assoc();
                echo '<a style="color:#FF3300;" href="comments.php?id='.$row['id'].'">Comments ('.$result2->num_rows.')</a>';
            }else{
                echo '<a style="color:#FF3300;" href="comments.php?id='.$row['id'].'">Comments (0)</a>';
            }
            echo '</div></div>';
            echo '<br style="margin: 0;padding: 0"/>';
          //  echo '<div class="box500 center500"><div style="font-weight: bold; line-height: 23px; font-family:\'sans-serif\'">';
            echo $row['Message'];
           // echo '</div></div>';
            echo '<br style="margin-bottom: 0px;padding: 0"/><span style="font-family: \'sans-serif\'; font-weight: bold; font-size: 15px">Fair Winds</span><br style="margin-bottom: 25px;padding: 0" />~<span style="font-family: \'sans-serif\'; font-weight: bold; font-size: 15px"> <i>The Crew @ Pirates Online Retribution</i></span><br style="margin-bottom: 8px;padding: 0"/>';
        //    echo '<div class="sep"></div>';
       //   }
          echo '<br><br><br><br>';
        }
    ?>
</div>
<?php include 'sections/footer.php';?>
