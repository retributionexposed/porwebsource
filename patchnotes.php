<?php include 'sections/header.php'; ?>
<style>

.conten_footer {
    position: absolute;
    background-repeat: repeat-y;
    height: 335px;
    object-fit: cover;
    z-index: 11;
    margin-left: -26px;
    width:860px;
    bottom: 0;
}
.content_page {
    position: relative;
    background-repeat: repeat-y;
    width: 800px;
    line-height: 30px;
    object-fit: fill;
    margin: 0 auto 0 auto;
    z-index: 0;
    margin-top: -100px;
    padding: 100px 10px 0 50px;
    text-align: left;
}
h2 {
	font-size: 2.5rem;

}
h1, h2 {
	margin: 0 auto;
	text-align: center;
}
h2, h3 {
	color: #685137;
}
.post-table>p:first-of-type {
	margin-bottom: 5px;
}


section {
	color: #675540;
	padding-bottom: 20px;
}

.kek {
	color: #745B3E;
	cursor: default;
	font-size: 3.75rem;
	line-height: 75px;
	min-height: 100px;
	padding: 45px 0 10px;
	text-shadow: 2px 4px 3px rgba(0,0,0,.3);
	width: 90%;
}

.newsList {
	background: rgba(30,30,30,.7);
	border: 1px solid #000;
	border-radius: 5px;
	color: #eee;
	line-height: 1.4;
	margin-bottom: 8px;
	min-height: 40px;
	width: 65%;
}

h3 {
	color: #745B3E;
	margin-top: 50px;
	font-size: 70px;
	text-shadow: 2px 4px 3px rgba(0,0,0,.4);
	padding: 0px;

}


h2 {
	font-size: 2.5rem;

}

a.newsTi {
	color: #E3F6E6;
	text-shadow: #ffffff 0px 0px 4px;

}

a:hover {
	color: #b766f8;
	text-shadow: none;
}

.commentNum {
	color: #b766f8;
}

.center {
	text-align: center;
	margin: 0;
	padding: 0;
	left: 50%;
}

p {
	color: #fff;
	text-align: center;
	cursor: default;
	font-size: 20px;
}


.newsBox {
	color: #E3F6E6;
	text-align: center;
	text-shadow: #ffffff 0px 0px 6px;
	cursor: default;
	font-size: 18px;
	width: calc(100% / 2 - 1px);
	left: 50px;
}

.post-table {
	margin-bottom: 20px;
}

article {
	display: block;
}


#section_body {
	height: auto;
	margin-bottom: 15px;
	padding: 1px 10px 15px;
}
.center-text {
	text-align: center;
}



.imgKeK {
	left: 50px;
	margin: 10px;

}

h2.welcome {
	font-size: 20px;
	color: #AB7232;
	font-weight: normal;
	font-family: times new roman,times,serif;
	text-align: left;
}

.pageContainer {
	width: 40%;
	background: rgba(30, 30, 30, 0.7);
	border: 1px solid #000;
	border-radius: 5px;
}

h6 {
	position: relative;
	/*background: rgba(0,0,0,.2);
	box-shadow: 3px 3px 5px 0 rgba(0,0,0,.4);*/
	color: #fff;
	font-weight: lighter;
	font-size: 24px;
	margin: 0 0 0 5px;
}

.mBox {
	background: rgba(30,30,30,.7);
	border: 1px solid #000;
	border-radius: 5px;
	padding-bottom: 15px;
	margin-top: 15px;
	width: 70%;
}

.smallBox {
	background: rgba(30,30,30,.7);
	border: 1px solid #000;
	border-radius: 5px;
	padding-bottom: 15px;
	margin-top: 15px;
	width: 40%;
}

.selectBox {
	background: rgba(30,30,30,.7);
	border: 1px solid #000;
	border-radius: 5px;
	color: #fff;
	font-size: 15px;
	text-shadow: 2px 4px 3px rgba(0,0,0,.4);
	padding-right: 5%;
	text-align: center!important;
	text-align: center;
}

.small {
	width: 40%;
}

.m {
	height: auto;
}

.m span {
	position: relative;
	color: white;
	font-size: 21px;
	font-size: 23px;
	text-shadow: 2px 4px 3px rgba(0,0,0,.4);
	display: inline-table;
	padding-left: 0px;
	text-align: center;
	text-decoration: none;
	top: -4px;
}

.m a {
	color: white;
	font-size: 11px;
	font-size: 23px;
	text-shadow: 2px 4px 3px rgba(0,0,0,.4);
	display: inline-table;
	padding-left: 0px;
	text-align: center;
	text-decoration: none;
	top: -4px;
}

.m a:hover {
	color: #ba0000;
}

.mBox li {
	color: #fff;
	font-size: 23px;
	text-shadow: 2px 4px 3px rgba(0,0,0,.4);
	padding-right: 5%;
	text-align: center!important;
	margin-bottom: 5px;
	text-align: center;
	text-shadow: 2px 4px 3px rgba(0,0,0,.4);
}
.content_page h1, .h1 {
    position: relative;
    font-family: caribbean;
    font-stretch: extra-expanded;
    font-weight: lighter;
    font-size: 50px;
    margin: 0 0 0 5px;
}
.releasesVersionTitle {
	display: inline-table;
	color: #fff;
	font-size: 35px;
	text-shadow: 2px 4px 3px rgba(0,0,0,.4);
	line-height: 1.4;
	padding-left: 5%;
	padding-right: 5%;
	text-align: center!important;
	margin-bottom: 5px;
	text-align: center;
	border-bottom: 1px solid rgba(255,255,255,.7);
	text-shadow: 2px 4px 3px rgba(0,0,0,.4);
}

.releasesTitle {
	display: inline-table;
	color: #fff;
	font-size: 35px;
	text-shadow: 2px 4px 3px rgba(0,0,0,.4);
	line-height: 1.4;
	padding-left: 5%;
	padding-right: 5%;
	text-align: center!important;
	margin-bottom: 5px;
	text-align: center;
	border-bottom: 1px solid rgba(255,255,255,.7);
	text-shadow: 2px 4px 3px rgba(0,0,0,.4);
}


</style>
<div class="page_center">
<div style="position: relative; margin-top: -50px;">
</div>
<section>
<h1 class="skull_large" id="30" name="30" style="color:black;">Patch Notes</h1>
<br><br>
<script>
function newData() {
	var xhttp = new XMLHttpRequest();
	var link = "/pnpol.php?" + "releaseVersion=" + document.getElementById('releaseVersion').value;;
	var reqMethod = "POST";
	var async = true;
	xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4) {
			var responseData = JSON.parse(xhttp.responseText);
			var id = responseData.id;
			var date = responseData.date;
			var version = responseData.version;
			var content = responseData.notes;
			$('.mBox').fadeOut(500);

			setTimeout(function () {
				$('.releasesTitle').html("Pirates Online Retribution v" + version);
				$('.releaseData').attr('id', version);
				$('.releaseData').html(content);

			}, 500);

			$('.mBox').fadeIn();
		}
	}
	xhttp.open(reqMethod, link, async);
	xhttp.send();
}
</script>
		<?php

				$sql = "SELECT * FROM Tab_Updates ORDER BY id DESC";
				$result = $conn->query($sql) or die('Something has gone wrong, try again later');

				echo '<center><div class="smallBox" id="releaseBox"> ';
				echo '<div style="font-size: 25px;" class="releasesVersionTitle">Release Versions</div>';
				echo '<form method="POST" id="fetchRelease" name="fetchRelease" class="fetchRelease">';
				echo '<input type="hidden" name="token" value="' . session_id() . '"></input>';
				echo '<select class="selectBox" id="releaseVersion" onchange="newData()" name="releaseVersion" title="Release Notes">';

				if($result->num_rows > 0) {
					$queryRow = $result->fetch_assoc();

					$initNotes = $queryRow['Notes'];
					$initVersion = $queryRow['Version'];

					echo '<option value="'.$queryRow['Version'].'">'.$queryRow['Date'].' POR-v'.$queryRow['Version'].'</option>';
					while($row = $result->fetch_assoc()) {

						if ($row['Version'] != $initVersion) {
							echo '<option value="'.$row['Version'].'">'.$row['Date'].' POR-v'.$row['Version'].'</option>';
						}

					}
					echo '</select>';
					echo '<span>';
					echo '</span>';
					echo '</form>';
					echo '</div></center>';

					$initNotes = $queryRow['Notes'];
					$initVersion = $queryRow['Version'];

					echo '<center><div class="mBox" id="releaseBox"> ';
					echo '<div class="releasesTitle">Pirates Online Retribution v'.$queryRow['Version'].'</div>';
					echo '<br style="margin: 0;padding: 0"/>';
					echo '<div class="releaseData" id="'.$initVersion.'">';
					echo $initNotes;
					echo '</div></div></center>';

					echo '<br><br><br>';
				}
		?>

		</section>
</div>
 <?php include 'sections/footer.php';?>
