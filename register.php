<?php include 'sections/header.php'; ?>

<div class="page_center">
    <h1>Account Registration</h1>
    <div class="sep"></div>
    <img src="https://i.gyazo.com/9949849baea21ca252194baf0484dde8.png" style="width:15%;">
    <?php
    	if($_GET["e"] == 1){
    ?>
    	<br>
			<h2><font color="red">There is already an account with that Username</font></h2>
			<br>
    <?php
    	}else if($_GET["e"] == 2){
    ?>
    		<br>
			<h2><font color="red">There is already an account with that email</font></h2>
			<br>
	<?php
		}else if($_GET["e"] == 3){
    ?>
    		<br>
			<h2><font color="red">Please enter captcha</font></h2>
			<br>
	<?php
			}else if($_GET["e"] == 4){
	?>
			<br>
			<h2><font color="red">This IP has been banned. If you believe this was a mistake, please contact us at support@piratesonline.us</font></h2>
			<br>
	<?php
		}else{
			echo '<br>';
		}
	?>

    <form id="Form1" action="_r.php" method="post">
    <div style="width:535px;" align="right">
    	<label>Username: </label><input type="text" id="user" name="user"><br>
    	<i>Must be at least 6 characters</i>
    	<br><br>
      	<label>Email: </label><input type="text" id="email" name="email"><br>
      	<i>Used in case of forgotten password</i>
      	<br><br>
        <label>Confirm Email: </label><input type="text" id="emailconf" name="emailconf"><br><br>
      	<label>Password: </label><input type="password" id="pass" name="pass"><br>
      	<i>Must be at least 6 characters</i><br>
      	<div style="margin:5px;"></div>
      	<label>Confirm Password: </label><input type="password" id="conf" name="conf"><br>
      	<i>Case sensitive</i>
      	<br><br>
      	<div class="g-recaptcha" data-sitekey="6Ld86ikTAAAAAJ4wlfZ3R2BYRp9WGRd-q44-xDpA"></div><br>
      	<div class="button">
		<a href="#" onClick="Validate();return false;"><span>Register</span></a>
	</div>
    </div>
  </form>
</div>
<script>
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
function validUser(username){
	var regex = /^[a-zA-Z0-9_.+-]+$/;
	return regex.test(username);
}
function Validate(){
	var flgIsValid = false;
	var e = "";

	if($('#fn').val() == ""){
		e = e + "\nPlease Enter First Name";
	}
	if($('#ln').val() == ""){
		e = e + "\nPlease Enter Last Name";
	}
	if($('#user').val() == ""){
		e = e + "\nPlease Enter Username";
	}
	if(validUser($('#user').val()) == false){
		e = e + "\nUsername cannot have special characters or spaces";
	}
	if($('#email').val() == ""){
		e = e + "\nPlease Enter Email";
	}
	if(isEmail($('#email').val()) == false){
		e = e + "\nPlease Enter a Valid Email";
	}
  if($('#emailconf').val() == ""){
    e = e + "\nPlease Enter Confirmation Email";
  }
  if($('#email').val() != $('#emailconf').val()){
    e = e + "\nEmails do not match"
  }
	if($('#pass').val() == ""){
		e = e + "\nPlease Enter Password";
	}
	if($('#pass').val().length < 6){
		e = e + "\nPassword must be at least 6 characters";
	}
	if($('#conf').val() == ""){
		e = e + "\nPlease Enter Confirmation Password";
	}

	if($('#pass').val() != $('#conf').val()){
		e = e + "\nPasswords do not match"
	}

	if(e == ""){
		flgIsValid = true;
	}

	if(flgIsValid){
		$('#Form1').submit();
	}else{
		alert("There are the following issues:" + e);
	}
}
</script><br><br><br><br>
<?php include 'sections/footer.php';?>
