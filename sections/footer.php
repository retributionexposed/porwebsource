            <div class="conten_footer">&nbsp;
                <?php
                if(isset($options['ship'])):?>
                    <div style="position: absolute; width: 369px; bottom:0">
                        <img src="images/footer_ship.png" />
                    </div>
                <?php endif;?>
            </div> <!-- page content footer image -->
        </div> <!-- page_content -->
    </div> <!-- content -->
</div> <!-- header's id=container -->
<div class="footer_wrapper">
    <div class="footer site_center">
        <div class="partners">
    <a href="https://piratesmc.net/"><img src="images/potcomc.png"></a> <a href="https://www.facebook.com/PotcoMemories/"><img src="images/hollyrogers.png"></a> <a href="https://www.youtube.com/user/TMPCast"><img src="https://i.imgur.com/RCaqj9s.png" style="padding-bottom:25px" width="156"></a>
  </div><br><br><br><br><br><br><br>
  <div class="social-icons">
    <a href="https://www.facebook.com/POTCORetribution/"><span></span></a>
    <a href="https://twitter.com/POR_Pirates"><span></span></a>
    <a href="https://www.youtube.com/user/POTCOGuides"><span></span></a>
    <a href="https://www.instagram.com/pirates_online_retribution/"><span></span></a>
    <a href="https://piratesforums.com/misc/contact"><span></span></a>
  </div>
        <div class="foot_msg">
            <p>
                <span style="font-size:12px;font-family:times new roman,times,serif; text-shadow: none; font-style: normal;">By using the site and the game you agree to our <a href="terms.php" style="color:red;">Terms of Service.</a><br>
                  © Pirates Online Retribution 2017.</span>


            </p>
        </div>

<div class="menu-item" id="menu-the-game" data-reactid="19"><a href="/" class="menu-item__link" data-reactid="20"><span class="menu-item__text" data-reactid="23">Home</span></a></div>

<div class="menu-item" id="menu-the-game" data-reactid="19"><a href="https://piratesforums.com" target="_blank" class="menu-item__link" data-reactid="20"><span class="menu-item__text" data-reactid="23">Forums</span></a></div>

<div class="menu-item" id="menu-the-game" data-reactid="19"><a href="faq.php" class="menu-item__link" data-reactid="20"><span class="menu-item__text" data-reactid="23">FAQ</span></a></div>

<div class="menu-item" id="menu-the-game" data-reactid="19"><a href="thecode.php" class="menu-item__link" data-reactid="20"><span class="menu-item__text" data-reactid="23">The Code</span></a></div>

<div class="menu-item" id="menu-the-game" data-reactid="19"><a href="about.php" class="menu-item__link" data-reactid="20"><span class="menu-item__text" data-reactid="23">About</span></a></div>

<div class="menu-item" id="menu-the-game" data-reactid="19"><a href="login.php" target="_blank" class="menu-item__link" data-reactid="20"><span class="menu-item__text" data-reactid="23">Login</span></a></div>

<div class="menu-item" id="menu-the-game" data-reactid="19"><a href="contact.php" class="menu-item__link" data-reactid="20"><span class="menu-item__text" data-reactid="23">Contact</span></a></div>

<div class="menu-item" id="menu-the-game" data-reactid="19"><a href="news.php" class="menu-item__link" data-reactid="20"><span class="menu-item__text" data-reactid="23">News</span></a></div>

<div class="menu-item" id="menu-the-game" data-reactid="19"><a href="rewards-store" class="menu-item__link" data-reactid="20"><span class="menu-item__text" data-reactid="23">Game Store</span></a></div>

<br><br><br><br>

    </div>
</div>
</body>
</html>
