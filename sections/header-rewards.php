<?php include 'head-rewards.php';
      include '../db.php' ?>
<div id="container">
<header>
    <?php
        $sql = "SELECT Alert FROM Tab_BannerAlerts";
        $result = $conn->query($sql) or die();
        $size = $result->num_rows;
        if($result->num_rows > 0){
    ?>
            <style>
            .alert {
                border-radius: 10px;
                background: rgba(136,3,3,.6);
                width: 85%;
                align:center;
                height:45px;
                margin: auto;
                color: #ffe769;
            }
            .alertText {
                position: relative;
                font-family: "Crimson Text", serif;
                font-stretch:extra-expanded;
                font-weight: lighter;
                font-size: 24px;
                margin: 0 0 0 0px;
                color: #e5c364;
                line-height: 40px;
            }
            </style>
            <div align="center" class="alert">
            <?php
                $i = 1;
                while($row = $result->fetch_assoc()){
                    echo '<div id="alert-' . $i . '"><h1 class="alertText">' . $row["Alert"] . '</h1></div>';
                    $i = $i + 1;
                }
            ?>
            </div>
            <br><br>
    <?php
        }
    if($size > 1){
    ?>
    <script>
        var divs = $('div[id^="alert-"]').hide(),
        i = 0;

        (function cycle() {

            divs.eq(i).fadeIn(400)
                      .delay(3000)
                      .fadeOut(400, cycle);

            i = ++i % divs.length;

        })();
    </script>
    <?php
    }
    ?>
    <div class="header">
        <div>
            <div class="clock">
                <div style="margin-left: 44px;">
                <!--    <h2 style="font-size: 14px; line-height: 19px ;color:#AB7232; font-weight:normal; font-family:times new roman">Time left until Pirates Online Retribution's Open Beta release!</h2> !-->
                </div>
                <div>
                    <div style="position:relative; padding: 0; text-align: left; margin: -6px 0 0 49px;">
              <!--      <img src="images/clock.png" /> !-->
                    </div>
                    <div style="position: relative; margin: -10px 0 0 -8px;">
                    <iframe scrolling="no" frameborder="0" width="145" height="50" src=""></iframe>
                    </div>
                </div>
            </div>
            <div style="float: left; width: 500px;">
                <a href="http://piratesonline.us/">
				<div class="banner">
				</div>
				</a>
            </div>
            <div style="float: left; width: 270px; margin-top: 8px;">
                <div style="margin-left: 90px; margin-top: 20px;">
                <?php
                    session_start();
                    $sql = "SELECT UserName FROM Tab_Reg WHERE id = '" . $conn->real_escape_string($_SESSION["id"]) . "'";
                    $result = $conn->query($sql);

                    if(!$result){
                        die ('Something has gone wrong, try again later');
                    }

                    if($result->num_rows > 0){
                        $row = $result->fetch_assoc();
                    ?>
                           <h2 class="welcome">Welcome, <?php echo $row["UserName"];?></h2>
                    <div class="button">
                      <a href="../account.php"><span>Account</span></a>
                    </div>
                    <div style="margin:20px;"></div>
                    <div class="button">
                       <a href="../logout.php"><span>Logout</span></a>
                     </div>
                 <?php
                    }else{
                ?>
                    <h2 class="welcome">Welcome, Pirate!</h2>
                    <div class="button">
                      <a href="../login.php"><span>Login</span></a>
                    </div>
                    <div style="margin:20px;"></div>
                    <div class="button">
                       <a href="../register.php"><span>Register</span></a>
                     </div>
                     <?php } ?>
                </div>
            </div>
            <div id="DiscordWidget" style="right: -354px; top: 330px; z-index: 99999;">
<img src="https://piratesforums.com/screenshots/fpzr01c.png" class="discordIcon" id="discordPic">
<div id="tspanein">
    <iframe src="https://discordapp.com/widget?id=223265238537994240&amp;theme=dark" width="350" height="500" allowtransparency="true" frameborder="0"></iframe>
  </div>
        </div>
        </div>
        <div style="clear: both"></div>
    </div>
</header>
<div class="content">
    <div class="content_header">
        <?php include 'nav-rewards.php';?>
    </div>
    <div class="content_page" <?php if(isset($options['no_content_page'])):?> style="padding-bottom: 0;"<?php endif;?>>
