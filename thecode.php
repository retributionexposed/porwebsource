<?php include 'sections/header.php'; ?>

<div class="page_center">
  <img src="https://i.imgur.com/JieEV9J.png" style="width: 50%;"/>
    <h1  class="skull" style="font-size: 40px;">THE PIRATE'S CODE</h1><br>
    <div class="sep"></div>

    <h2 style="font-size: 17px;">Everyone deserves to be treated nicely. If somebody isn't being nice, file a report against them.</h2>

    <h2 style="font-family: pirates; font-size: 40px;">Treat others with respect</h2>

    <h2 style="font-size: 17px;">This one is rather self explanatory. Treat others the way you expect them to treat you. If you feel that somebody is being overly offensive to another player, or to you, please report them</h2>

    <h2 style="font-family: pirates; font-size: 40px;">Watch yer mouth</h2>

    <h2 style="font-size: 17px;">Any form of profanity or excessively rude language towards any other member of our community is not permitted on Pirates Online Retribution under any circumstances!</h2>

    <h2 style="font-family: pirates; font-size: 40px;">Cheaters will fail, and end up in jail</h2>

    <h2 style="font-size: 17px;">Any form of cheating, exploiting, or use of third party programs is not allowed. If you find an exploit, report it by emailing us at support@piratesonline.us.</h2>

    <h2 style="font-family: pirates; font-size: 40px;">Be a considerate person</h2>

    <h2 style="font-size: 17px;">Any form of spam, harassment, or making others feel upset or uncomfortable, will most likely result in a warning by a staff member. If this behavior persists, the staff will escalate the issue and harsher consequences will be given.</h2>

    <h2 style="font-family: pirates; font-size: 40px;">Privacy matters</h2>

    <h2 style="font-size: 17px;">At Pirates Online Retribution, nobody needs to know anything about you, aside from the fact that you're a pirate. Do not share any personal information about yourself - especially phone numbers, home addresses, and your account username or password. If somebody asks about this information, just nicely say "Sorry, I can't." and report this player for requesting personal information.</h2>

    <br />
<br><br>
    <h1 style="font-size: 60px; font-family: pirates;">Naming Guidelines</h1>
<br><div class="sep"></div>
    <ul class="name_guidelines" style="padding-bottom: 50px;">
      <li>No personally identifiable information. For example: your full name.</li>
      <li>No &#34;username&#34; looking names. For example: gennam64.</li>
      <li>No celebrities, bands, or sports teams.</li>
      <li>No names of major public figures. For example: President Trump.</li>
      <li>No copyrights or TM brands.</li>
      <li>No Disney characters or other cartoon characters.</li>
      <li>No exact characters from the Pirates movies (explicit or implied), and no mixing of exact character names. For example: Elizabeth Turner, William Sparrow.</li>
      <li>No phrases or sentences.</li>
      <li>Names must be 4 words maximum, have at least three letters, contain a vowel, and must not be a single letter name.</li>
      <li>No word merging. For example: JoePirate.</li>
      <li>No inappropriate mixed case or all caps. For example: JoePirate, joEpiRAte, JOEPIRATE.</li>
      <li>No spoonerisms for any of these rules. For example: Squarebob Spongepants.</li>
      <li>No backwards spellings for any of these rules. For example: Worraps Kcaj.</li>
      <li>No numbers (digits or spelled out). For example: PirateEightTwentyFour, Pirate645</li>
      <li>No empty names or all whitespace.</li>
      <li>No L33t-5p34K or ascii. For example: J4CK 5P4RR0W, :D, :(.</li>
      <li>No web sites. For example: Disney.com, piratesonline.us.</li>
      <li>No foul, violent, hateful or suggestive language.</li>
      <li>No references to anything that is illegal or not family friendly.</li>
      <li>No explicit religious references.</li>
      <li>Periods and Dashes are the only punctuation/symbols allowed, but must be separated by a letter. For example: Joe&#39;s Pirate and Pirate-Anne.</li>
      <li>No described relationships. For example: Alvin Freckle&#39;s Mom, Thaddeus Plunderweather&#39;s Friend.</li>
      <li>No names that imply that you&#39;re a Disney or POR employee. For example: POR Developer Daniel Alves.</li>
    </ul><br>
    <h1 style="font-size: 60px; font-family: pirates;">Terms of Service</h1>
<br><div class="sep"></div>
<center>
<div id="terms">
<p>By using Pirates Online Retribution, you certify that you understand that Pirates Online Retribution is not associated with The Walt Disney Company and/or the Disney Interactive Media Group, and that you release The Walt Disney Company from any and all liability for any personal loss caused to you by the use of Pirates Online Retribution.
Pirates Online Retribution or POR for short is a free ongoing fan-made recreation of Disney's now-closed MMO, Pirates of the Caribbean Online. The purpose of this project is to "revive" the game, using what original files we have, making the game work exactly as it was before the game's closure on September 19th, 2013. All changes from the original (with few exceptions), whether it be adding unreleased content, re-adding content/features which was once apart of the game and later removed, or creating brand new content & features will all be handled after our "full" release as a post-closure community server (after alpha and beta stages).</p><br>

<p><strong>All donations made to Pirates Online Retribution via our Game’s Rewards Store are used solely for covering server hosting costs, and maintenance expenses. POR is, and will always be 100% nonprofit.</strong></p><br>

<p>POR reserves the right to terminate your access to our servers at any time, for any reason. These reasons include but are not limited to:</p><br>

<ul class="TOS">
   <li>Usage of cursing, inappropriate innuendos or any other form of inappropriate language.</li>
   <li>Submitting inappropriate names for review.</li>
   <li>Sending inappropriate emails to POR.</li>
   <li>Disrespecting, harassing or spamming others.</li>
   <li>Disrespecting, harassing or spamming POR staff.</li>
   <li>Intentionally evading chat filters</li>
   <li>Intentionally disrupting another players experience.</li>
   <li>Trolling</li>
   <li>Usage of third-party software to modify the game.</li>
   <li>Using automation systems or tools to control the game</li>
   <li>Attempting to compromise POR's computer systems.</li>
   <li>Attempting to compromise POR's servers.</li>
   <li>Connecting unofficial or custom clients to POR's servers.</li>
   <li>Distributing spyware, malware, keyloggers, or other malicious software to members of the POR community.
   </li>
   <li>Excessively exploiting a bug in POR to gain an advantage over other players or to harm other players
       experience.
   </li>
   <li>Sharing location, addresses, phone numbers, account information or any other form of personal information.
   </li>
   <li>Requesting location, addresses, phone numbers, account information or any other form of personal information
       from others.
   </li>
</ul><br>

<p>In the event where a user is locked out of their account, POR will always side with the account owner. Each
   POR account has an "account owner", defined by the person who is able to communicate over email with POR
   with the exact email address associated with your account.</p><br>

<p>In the event that the account owner chooses to share their login information with another person or another
   person controls a session of the game initiated with the account owner's knowledge, the account owner will be
   held liable for any violations of the Terms of Service and General Rules.</p>

<p>In the event where an account is suspended, terminated, or restricted, we reserve the right to take the same
   actions upon all accounts sharing an IP address.</p><br>

<p>In the event where an account is suspended, terminated, or restricted, the account owner may appeal the
   suspension, termination, or restrictions by emailing POR at support@piratesonline.us. Anything you write may
   be used as evidence against you in an appeal.</p><br>

<p>POR reserves the right to read all messages of any type that are sent across our network.</p><br>

<p>POR reserves the right to change these rules with or without notice.</p></center>
<br><br><br><br><br><br>
</div>

<br style="margin-bottom: 0px;padding: 0"/>
<?php include 'sections/footer.php'; ?>
