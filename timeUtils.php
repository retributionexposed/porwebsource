<?php

function getHumanTime($time) {
  $timeIntervals = array(
    array('weeks', 604800),
    array('days', 86400),
    array('hours', 3600),
    array('minutes', 60),
    array('seconds', 1)
  );
  
  $timeStrings = array();

  foreach ($timeIntervals as $timeInterval) {
 	  $timeName = $timeInterval[0];
	  $timeInterval = $timeInterval[1];
	  $value = floor($time / $timeInterval);
	
  	if ($value >= 1) {
	  	$time -= $value * $timeInterval;
		
		  if ($value === 1) {
        $timeName += 's';
  	  }
		
	    $timeStrings[] = $value . " " . $timeName;
    }
  }

  $count = count($timeStrings);

  if ($count >= 2) {
	  $count--;
	  $timeStrings[$count] = 'and ' . $timeStrings[$count];
  }

  return implode(', ', $timeStrings);
}
?>